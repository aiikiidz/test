myApp.config(function($routeProvider){
	
	$routeProvider
	
	.when('/', {
		templateUrl: 'page/login.html',
		controller: 'loginCtrl'
	})
	
	.when('/home', {
		templateUrl: 'page/home.html',
		controller: 'homeCtrl',
		authenticated: true,
	})

});

myApp.run(function($rootScope, $location, authFact){
	$rootScope.$on('$routeChangeStart', function(event, next, current){
		if(next.$$route.authenticated){
			var userAuth = authFact.getAccessToken();
			if (!userAuth){
				$location.path('/');
			}
		}
	});
});
























